package com.ibpd.henuocms.assist;
/**
 * 权限-模块的一个自定义类
 * @author mg by qq:349070443
 *
 */
public class PermissionModelEntity {

	public static final Integer MODEL_TYPE_SITE=0;
	public static final Integer MODEL_TYPE_NODE=1;
	public static final Integer MODEL_TYPE_FUNMODEL=2;
	
	private Long modelId;
	private String modelName;
	private Integer modelType;
	public Long getModelId() {
		return modelId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public Integer getModelType() {
		return modelType;
	}
	public void setModelType(Integer modelType) {
		this.modelType = modelType;
	}
}
