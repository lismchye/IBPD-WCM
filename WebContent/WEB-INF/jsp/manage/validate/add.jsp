<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <form id="fm" class="forms" action="<%=basePath%>manage/validate/doAdd.do" method="post">
    	<input type="hidden" name="id" id="id" value=""/>
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">验证模型名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="validateName" name="validateName" tip="输入验证模型名称" datatype="*3-10" errormsg="名称长度不得小于3且不得大于10"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型类型</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="validateType" name="validateType" tip="输入验证模型类型" datatype="n1-1"  errormsg="0-9的整数"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型表达式</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="dataType" name="dataType" tip="输入验证模型表达式" datatype="*1-10"  errormsg="参考validformde的tatype"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">异步验证的url地址</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="ajaxUrl" name="ajaxUrl" tip="输入有效的url地址" datatype="url"  errormsg="输入有效的url地址"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型比对标签</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="rechek" name="rechek" tip="输入验证模型比对标签的name属性" datatype="*1-10"  errormsg="输入验证模型比对标签的name属性"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证错误提示信息</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="errormsg" name="errormsg" tip="输入验证错误提示信息" datatype="*1-100"  errormsg="输入验证错误提示信息"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型附加javascript函数、代码</th>
    			<td class="val">
    				<textarea name="func" rows="10" id="func" tip="输入验证模型附加javascript函数、代码" datatype="*1-255"  errormsg="输入验证错误提示信验证模型附加javascript函数、代码"></textarea></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    	</table>
    	<input type="submit"/>&nbsp;&nbsp;<input type="button" value="关闭" onclick="javascript:$('#addDialog').window('close');"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>