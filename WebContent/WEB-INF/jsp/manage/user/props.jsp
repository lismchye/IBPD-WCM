<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		body{
			overflow-y:auto;
		}
		.baseInfo{
			
			
		}
		.baseInfo tr .tit .tittext{
			font-size:10px;
			overflow:hidden;
			text-overflow:clip;
			width:80px;
		}
		.baseInfo tr .val{
			padding:2 2 2 2;
			width:110px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr .val .int {
			width:80px;
		}
		.baseInfo tr .val .int input{
			width:80px;
		}
		.baseInfo tr .val .int select{
			width:80px;
		}
		.baseInfo tr .val .int textarea{
			width:80px;
		}
		.baseInfo tr .val .assist {
			width:20px;
		}
		.baseInfo tr .val .assist input {
			width:20px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	
		<input type="hidden" id="id" value="${entity.id }"/>
			
				<table class="baseInfo" cellpadding="0" cellspacing="0">
		<c:forEach items="${htmls }" var="html">
			<c:if test="${html.value.k=='base'}">
			<tr>
				<td class="tit">
					<div class="tittext">${html.key }</div>
				</td>
				<td class="val">
					${html.value.v }
				</td>		
			</tr>
			</c:if>
		</c:forEach>
				</table>

	
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		bindEvents();
		$("[mthod='smt']").bind("blur",function(e){
			var nodeId=$("#id").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("value");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/User/saveProp.do",
				{id:nodeId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
	});   
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){parent.showFileSelecterDialog(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*上级页面调用这个函数来做到设置本页 tag值的效果   参数：name为本页tag的name值，val为具体值,具体调用效果可参阅node的index.jsp页面
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
     </script>
     
	</body>
</html>
