package com.ibpd.henuocms.web.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.hsqldb.lib.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController;
@Controller
public class WebSite  extends BaseController{
	@RequestMapping("Web/SubSite/index.do")
	public String index(org.springframework.ui.Model model,Long siteId,String type,HttpServletRequest req){
		if(siteId==null){
			model.addAttribute(ERROR_MSG,"站点ID为空");
			return super.ERROR_PAGE;			
		}
		ISubSiteService siteService=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity site=siteService.getEntityById(siteId);
		if(site==null){
			model.addAttribute(ERROR_MSG,"没有找到这个站点");
			return super.ERROR_PAGE;
		}
		Long tempId=site.getSitePageTemplateId();
		IPageTemplateService pt=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		PageTemplateEntity pageTemplate=null;
		if(tempId==-1L){
			pageTemplate=pt.getDefaultPageTemplate(PageTemplateEntity.TEMPLATE_TYPE_SITE);
		}else{
			pageTemplate=pt.getEntityById(tempId); 
		} 
		if(pageTemplate==null){
			model.addAttribute(ERROR_MSG,"没有找到 默认模板");
			return super.ERROR_PAGE;
		}
		IStyleTemplateService styleTempService=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		Long styleTempId=site.getSiteStyleTemplateId();
		StyleTemplateEntity styleTemplate=null;
		if(styleTempId==-1L){
			styleTempId=pageTemplate.getDefaultStyleTemplateId();
		}
		styleTemplate=styleTempService.getEntityById(styleTempId);
		if(styleTemplate==null){
			model.addAttribute(ERROR_MSG,"没有找到样式模板");
			return super.ERROR_PAGE;
		}
		//以上是准备内容的过程，下面是把具体的模板路径等映射到前台的过程
		String tmpPath=pageTemplate.getTemplateFilePath();
		if(StringUtil.isEmpty(tmpPath)){
			model.addAttribute(ERROR_MSG,"模板路径为空");
			return super.ERROR_PAGE;
		}
		String vdir=getVisualPathByType(req,type);
		model.addAttribute("siteTitle",site.getSiteName());
		model.addAttribute(PAGE_TITLE,site.getFullName());
		model.addAttribute("keywords",site.getKeywords());
		model.addAttribute("description",site.getDescription());
		model.addAttribute("copyright",site.getCopyright());
		model.addAttribute("pageTemplatePath",vdir+"template/"+pageTemplate.getTemplateFilePath()+"/");
		model.addAttribute("styleTemplatePath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/");
		model.addAttribute("cssPath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/style.css");
		return "template/"+pageTemplate.getTemplateFilePath()+"/index";
	}
}
