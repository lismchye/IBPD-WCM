package com.ibpd.henuocms.web.controller.manage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.UnZip;
import com.ibpd.henuocms.entity.PaperEntity;
import com.ibpd.henuocms.service.paper.IPaperService;
import com.ibpd.henuocms.service.paper.PaperServiceImpl;

@Controller
public class Paper extends BaseController {
	@Resource(name = "paperService")
	IPaperService paperService;
	@RequestMapping("manage/paper/index.do")
	public String index() throws IOException{
		return "manage/paper/index";
	}	
	@RequestMapping("manage/paper/doAdd.do")
	public String doAdd(String paperName,Integer type,Integer order,String publishDir,String intro,HttpServletRequest req) throws IOException{
		if(new File(req.getRealPath("/")+File.separatorChar+"publish"+File.separatorChar+publishDir).exists()){
			return ERROR_PAGE;
		}
		paperService=(IPaperService) ((paperService==null)?ServiceProxyFactory.getServiceProxy(PaperServiceImpl.class):paperService);
		paperService.setDao(new DaoImpl());
		PaperEntity pe=new PaperEntity();
		pe.setCreateTime(new Date());
		pe.setIntro(intro);
		pe.setOrder(order);
		pe.setPaperName(paperName);
		pe.setPublishDir(publishDir);
		pe.setType(type);
		pe.setUpdateTime(new Date());
		paperService.saveEntity(pe);
		UnZip.unZipFiles(req.getRealPath("/")+File.separatorChar+"WEB-INF"+File.separatorChar+"periodical.zip",req.getRealPath("/")+File.separatorChar+"publish"+File.separatorChar+publishDir+File.separatorChar);
		//设置页面标题
//		String indexFilePath=req.getRealPath("/")+File.separatorChar+"publish"+File.separatorChar+publishDir+File.separatorChar+"index.html";
//		
//		BufferedReader br = new BufferedReader(new FileReader(indexFilePath));  
//		String data = br.readLine()+"/r/n";//一次读入一行，直到读入null为文件结束  
//		while( data!=null){  
//		      data += br.readLine()+"/r/n"; //接着读下一行  
//		}  
//		br.close();
//		data.replace("${page_title}", paperName);
//		System.out.println(data);
//		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(indexFilePath));    
//        osw.write(data,0,data.length());    
//        osw.flush();    
		return "redirect:/manage/paper/index.do";
	}
	@RequestMapping("manage/paper/doEdit.do")
	public String doEdit(Long id,String paperName,Integer type,Integer order,String publishDir,String intro) throws IOException{
		paperService=(IPaperService) ((paperService==null)?ServiceProxyFactory.getServiceProxy(PaperServiceImpl.class):paperService);
		paperService.setDao(new DaoImpl());
		PaperEntity pe=paperService.getEntityById(id);
		if(pe!=null){
			pe.setIntro(intro);
			pe.setOrder(order);
			pe.setPaperName(paperName);
			pe.setPublishDir(publishDir);
			pe.setType(type);
			pe.setUpdateTime(new Date());
			paperService.saveEntity(pe);
		}else{
			return super.ERROR_PAGE;
		}
		return "redirect:/manage/paper/index.do";
	}
	@RequestMapping("manage/paper/doDel.do")
	public String doDel(String ids) throws IOException{
		paperService=(IPaperService) ((paperService==null)?ServiceProxyFactory.getServiceProxy(PaperServiceImpl.class):paperService);
		paperService.setDao(new DaoImpl());
		paperService.batchDel(ids);
		return "redirect:/manage/paper/index.do";
	}
	@RequestMapping("manage/paper/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String modelId,String order,Integer page,Integer rows,String sort,String queryData){
		paperService=(IPaperService) ((paperService==null)?ServiceProxyFactory.getServiceProxy(PaperServiceImpl.class):paperService);
		paperService.setDao(new DaoImpl());
		super.getList(req, paperService, resp, order, page, rows, sort, queryData);
	}
}
