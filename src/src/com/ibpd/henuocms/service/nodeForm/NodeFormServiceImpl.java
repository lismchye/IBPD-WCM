package com.ibpd.henuocms.service.nodeForm;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.dao.impl.HqlParameter.DataType_Enum;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.NodeFormEntity;
import com.ibpd.henuocms.entity.NodeFormFieldEntity;
import com.ibpd.henuocms.entity.NodeNodeFormEntity;
import com.ibpd.henuocms.entity.NodeFormFieldEntity.TagType;
import com.ibpd.henuocms.entity.ext.NodeFormExtEntity;
@Transactional
@Service("nodeFormService")
public class NodeFormServiceImpl extends BaseServiceImpl<NodeFormEntity> implements INodeFormService {
	private Logger log=Logger.getLogger(this.getClass());
	public NodeFormServiceImpl(){
		super();
		this.tableName="NodeFormEntity";
		this.currentClass=NodeFormEntity.class;
		this.initOK();
	} 

	public static void main(String[] args){
		NodeFormServiceImpl n=new NodeFormServiceImpl();
		n.initDefaultNodeForm();
		n.initDefaultContentForm();
//		n.aaa();
	}

	public void initDefaultContentForm() {
		NodeFormEntity nForm=new NodeFormEntity();
		nForm.setCreateTime(new Date());
		nForm.setUpdateTime(new Date());
		nForm.setFormName("默认内容表单");
		nForm.setFormType(NodeFormEntity.FORMTYPE_CONTENTFORM);
		nForm.setIsDefault(true);
		nForm.setOrder(0);
		INodeFormService formService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
		for(NodeFormEntity n:formService.getList("from "+formService.getTableName()+" where formType="+NodeFormEntity.FORMTYPE_CONTENTFORM + " and isDefault=true",null)){
			formService.deleteByPK(n.getId());
		}
		formService.saveEntity(nForm);
		log.debug("NodeFormEntity.id="+nForm.getId());
		
		INodeFormFieldService nodeFormFieldService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
		List<NodeFormFieldEntity> nfList=nodeFormFieldService.getList("from "+nodeFormFieldService.getTableName()+" where nodeFormId="+nForm.getId(),null);
		for(NodeFormFieldEntity nf:nfList){
			nodeFormFieldService.deleteByPK(nf.getId());
		}
		initNewContentFormFields(nForm.getId());
	}
	/**
	 * 初始化栏目对应的内容类型的字段
	 * @param nodeFormId
	 */
	private void initNewContentFormFields(Long nodeFormId){
		INodeFormFieldService nodeFormFieldService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
		Method[] methods=ContentEntity.class.getMethods();
		Integer ord=0;
		for(Method m:methods){
			String name=m.getName();
			String type=m.getReturnType().getSimpleName();
			if(name.substring(0,3).equals("get")){
				name=name.replace("get", "");
				name=name.substring(0,1).toLowerCase()+name.substring(1);
				if(!checkFieldIsEditable(name))
					continue;
				if(name.equals("class"))
					continue;
				if(name.equals("text"))
					continue;
				log.debug(name+"\t"+type);
				NodeFormFieldEntity field=new NodeFormFieldEntity();
				field.setAssistTags(getAssistTags(name));
				field.setCreateTime(new Date());
				field.setDefaultValue(getDefaultValue(name,type));
				field.setDisplayByAddMode(true);
				field.setDisplayByEditMode(true);
				field.setDisplayName(name);
				field.setExpGroup("");
				field.setFieldDescription("");
				field.setFieldName(name);
				field.setGroup("");//内容暂时不做分组
				field.setNodeFormId(nodeFormId);
				field.setOptionalValue(getOptionalValue(name,type));
				field.setOrder(ord++);
				field.setTagType(getTagType(name,type));
				field.setUpdateTime(new Date());
				field.setWriteByAddMode(true);
				field.setWriteByEditMode(true);
				field.setIsEditable(true);
				nodeFormFieldService.saveEntity(field);
			}
		}

	}
	public void initDefaultNodeForm() {
		NodeFormEntity nForm=new NodeFormEntity();
		nForm.setCreateTime(new Date());
		nForm.setUpdateTime(new Date());
		nForm.setFormName("默认栏目表单");
		nForm.setFormType(NodeFormEntity.FORMTYPE_NODEFORM);
		nForm.setIsDefault(true);
		nForm.setOrder(0);
		INodeFormService formService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
		for(NodeFormEntity n:formService.getList("from "+formService.getTableName()+" where formType="+NodeFormEntity.FORMTYPE_NODEFORM + " and isDefault=true",null)){
			formService.deleteByPK(n.getId());
		}
		formService.saveEntity(nForm);
		log.debug("NodeFormEntity.id="+nForm.getId());
		
		INodeFormFieldService nodeFormFieldService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
		List<NodeFormFieldEntity> nfList=nodeFormFieldService.getList("from "+nodeFormFieldService.getTableName()+" where nodeFormId="+nForm.getId(),null);
		for(NodeFormFieldEntity nf:nfList){
			nodeFormFieldService.deleteByPK(nf.getId());
		}
		initNewNodeFormFields(nForm.getId());
	}
	private Integer getTagType(String name,String type){
		if(type.toLowerCase().equals("boolean")){
			return TagType.STATIC_COMBO.getValue();
		}else{
			if(name.equals("state")){
				return TagType.STATIC_COMBO.getValue();
			}else if(name.equals("nodeType")){
				return TagType.STATIC_COMBO.getValue();
			}
		}
		return TagType.STATIC_TEXT.getValue();
	}
	private String getOptionalValue(String name,String type){
		if(type.toLowerCase().equals("boolean")){
			return "1:是;0:否";
		}else{
			if(name.equals("state")){
				return "1:开通;0:关闭";
			}else if(name.equals("nodeType")){
				return "1:外部指向;0:普通栏目";
			}else{
				return "";
			}
		}
	}
	private String getDefaultValue(String name,String type){
		if(type.toLowerCase().equals("boolean")){
			return "0";
		}else{
			if(name.equals("state") || name.equals("nodeType")){
				return "0";
			}else{
				return "";
			}
		}
		
	}
	private String getAssistTags(String name){
		name=","+name+",";
		String fileSelecter=",map,icon,banner,bgMusic,logo,linkUrl,";
		String pageTemplate=",templateGroupId,contentpageTemplateId,nodeStyleTemplateId,commentpageTemplateId,";
		String styleTemplate=",commentStyleTemplateId,nodeStyleTemplateId,contentStyleTemplateId,";
		String formSelect=",contentMediaId,nodeMediaId,";
		String userSelect=",manageUser,";
		if(fileSelecter.indexOf(name)!=-1){
			return NodeFormFieldEntity.AssistType.FileSelecter.toString();
		}else if(pageTemplate.indexOf(name)!=-1){
			return NodeFormFieldEntity.AssistType.PageTemplateSelecter.toString();
		}else if(styleTemplate.indexOf(name)!=-1){
			return NodeFormFieldEntity.AssistType.StyleTemplateSelecter.toString();
		}else if(formSelect.indexOf(name)!=-1){
			return NodeFormFieldEntity.AssistType.FormSelecter.toString();
		}else if(userSelect.indexOf(name)!=-1){
			return NodeFormFieldEntity.AssistType.UserSelecter.toString();
		}else
			return "";
	}

	private String getFromStr(){
		return "from "+this.getTableName();
	}
	public NodeFormExtEntity getNodeForm(Integer formType, Long nodeId,Long formId) {
		//如果ID为-1，那么就取默认的formField
		nodeId=(nodeId==null)?-1L:nodeId;
		formId=(formId==null)?-1L:formId;
		if(nodeId==-1L && formId==-1L){
			NodeFormEntity defaultForm=getDefaultNodeForm(NodeFormEntity.FORMTYPE_NODEFORM);
			if(defaultForm==null){
				return null;
			}
			INodeFormFieldService nffService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
			List<NodeFormFieldEntity> nffList=nffService.getList("from "+nffService.getTableName()+" where nodeFormId="+defaultForm.getId(),null,"order asc",1000,1);
			NodeFormExtEntity nfe=new NodeFormExtEntity(defaultForm,nffList);
			return nfe;
			
		}
		//先从nodeForm中找，这里找到的 是栏目本身创建且具有编辑权限的表单，如果这里找不到，再从 对照里找引用的 
		List<NodeFormEntity> nodeFormList=null;
		if(nodeId>0){
			nodeFormList=this.getList(getFromStr()+" where nodeId="+nodeId+" and formType="+formType, null);
		}else if(formId>0){
			nodeFormList=this.getList(getFromStr()+" where id="+formId, null);
		}
		if(nodeFormList!=null && nodeFormList.size()>0){
			if(nodeFormList.size()>1)
				log.error("找到多于一条的栏目对应表单：nodeId="+nodeId+" formType"+formType);
			//不管有几个，这里只取第一个
			INodeFormFieldService nffService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
			List<NodeFormFieldEntity> nffList=nffService.getList("from "+nffService.getTableName()+" where nodeFormId="+nodeFormList.get(0).getId(),null,"order asc",1000,1);
			NodeFormExtEntity nfe=new NodeFormExtEntity(nodeFormList.get(0),nffList);
			return nfe;
			
		}
		//如果在nodeForm里找不到，则说明该栏目尚未定义表单或者是引用了别的表单，这里讲进行检索，如果为做引用，则说明没有定义表单，否则 返回已经引用的表单
		INodeNodeFormService nns=(INodeNodeFormService) ServiceProxyFactory.getServiceProxy(NodeNodeFormServiceImpl.class);
		List<HqlParameter> hqlParameterList=new ArrayList<HqlParameter>();
		hqlParameterList.add(new HqlParameter("nodeId",nodeId,DataType_Enum.Long));
		List<NodeNodeFormEntity> nnEntityList=nns.getList("from "+nns.getTableName()+" where nodeId=:nodeId", hqlParameterList);
		if(nnEntityList==null){
			return null;//没找到对应的，这里 先返回空，然后调用方再活取默认的表单,或者新建一个表单等逻辑
		}else{
			if(nnEntityList.size()==0)
				return null;
			//如果存在对照，则找出具体form和fields
//			List<HqlParameter> formHqlParams=new ArrayList<HqlParameter>();
//			formHqlParams.add(new HqlParameter("formType",formType,DataType_Enum.Integer));
			
			String hql="from "+this.getTableName()+" where formType="+formType+" and (";
			for(NodeNodeFormEntity nnForm:nnEntityList){
				//取出nodeId并从数据库中读取具体的form，然后从form中读取fields
				hql+="id="+nnForm.getFormId()+" or ";
			}
			hql=hql.substring(0,hql.length()-4)+")";
			log.debug(hql);
			List<NodeFormEntity> nfList=this.getList(hql, null,"order asc",1000,0);
			//这里应该只能查出一条记录，但 不排除 某些情况下会有 多条记录的情况
			if(nfList==null || nfList.size()==0){
				nfList=new ArrayList<NodeFormEntity>();
				NodeFormEntity defaultForm=this.getDefaultNodeForm(formType);
				if(defaultForm==null)
					return null;
				else{
					nfList.add(defaultForm);
				}
			}
			//确保有内容
			//如果有 大于一条的内容，则先 输出一个调试信息，实际上不应该有 多条对应的条目
			if(nfList.size()>1)
			log.error("存在多个对应的form");
			//不管有几个，这里只取第一个
			INodeFormFieldService nffService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
			List<NodeFormFieldEntity> nffList=nffService.getList("from "+nffService.getTableName()+" where nodeFormId="+nfList.get(0).getId(),null,"order asc",1000,1);
			NodeFormExtEntity nfe=new NodeFormExtEntity(nfList.get(0),nffList);
			return nfe;
		}
	}
	public Long getNodeFormId(Integer formType, Long id) {
		NodeFormExtEntity e=getNodeForm(formType,id,null);
		if(e==null)
			return null;
		else
			return e.getId();
	}
	/**
	 * 链接
	 */
	public void linkNodeForm(Long nodeId, Long nodeFormId) {
		INodeNodeFormService nns=(INodeNodeFormService) ServiceProxyFactory.getServiceProxy(NodeNodeFormServiceImpl.class);
		NodeNodeFormEntity nodeNode=new NodeNodeFormEntity();
		nodeNode.setFormId(nodeFormId);
		nodeNode.setNodeId(nodeId);
		nns.saveEntity(nodeNode);
	}
	/**
	 * 获取nodeForm列表
	 * @param pageIndex
	 * @param pageSize
	 * @param orderBy 排序 ：格式为：id desc
	 * @param filter 直接就是where后面的语句,这个参数主要为了方便前台调用方分别显示栏目类型、内容类型或其他类型表单列表是用，以及过滤显示的时候 是用
	 * @return
	 */
	public List<NodeFormEntity> getNodeFormList(Integer pageIndex,Integer pageSize,String orderBy,String filter){
		return this.getList("from "+this.getTableName()+" where "+filter, null, orderBy, pageSize, pageIndex);
	}
	/**
	 * 获取默认表单可以分为栏目的默认表单和内容的默认表单
	 * @param formType
	 * @return
	 */
	public NodeFormEntity getDefaultNodeForm(Integer formType){
		if(formType==null){
			log.error("getDefaultNodeForm 函数 中，formType没有传值.");
			formType=NodeFormEntity.FORMTYPE_NODEFORM;
		}
		List<NodeFormEntity> nList=this.getList("from "+ getTableName()+" where isDefault=true and formType="+formType,null);
		if(nList==null)
			return null;
		else if(nList.size()>=1){
			log.error("多于一条的默认表单");
			return nList.get(0);
		}else{
			return null;
		}
	}
	/**
	 * 更新字段信息
	 * @param formField
	 */
	public void updateFieldInfo(NodeFormFieldEntity formField){
		if(formField==null){
			log.error("formField实体对象为空");
			return;
		}else if(formField.getId()==null || formField.getId()==-1L){
			log.error("formField id 为空");
			return;
		}else{
			INodeFormFieldService nodeFormFieldService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
			nodeFormFieldService.saveEntity(formField);
		}
	}
	/**
	 * 添加 一个 新的表单
	 * @param nodeId
	 * @param nodeForm
	 */
	public void addNewNodeForm(Long  nodeId,NodeFormEntity nodeForm,Integer type){
		//取消对照
		UnLinkNodeForm(nodeId,type);
		nodeForm.setNodeId(nodeId);
		this.saveEntity(nodeForm);
		//注意：这里不做node-nodeform对照
		//下面是做node-nodeForm对照的信息，现在取消了，对栏目获取form的逻辑做了调整，由原来的全部从node-nodeform对照中获取改为优先从nodeFOrm中获取（具有编辑权限），如果找不到再从对照中获取，这里获取到的没有编辑权限，仅仅是引用
//		INodeNodeFormService s=(INodeNodeFormService) ServiceProxyFactory.getServiceProxy(NodeNodeFormServiceImpl.class);
//		NodeNodeFormEntity n=new NodeNodeFormEntity();
//		n.setFormId(nodeForm.getId());
//		n.setNodeId(nodeId);
//		s.saveEntity(n);
		if(type==NodeFormEntity.FORMTYPE_NODEFORM)
			initNewNodeFormFields(nodeForm.getId());
		else
			initNewContentFormFields(nodeForm.getId());
		linkNodeForm(nodeId, nodeForm.getId());
	}
	/**
	 * 更新表单的基本信息
	 * @param nodeForm
	 */
	public void updateNodeFormBaseInfo(NodeFormEntity nodeForm){
		this.updateEntity(nodeForm);
	}
	/**
	 * 初始化新添加表单的对应字段信息
	 * @param nodeFormId
	 */
	private void initNewNodeFormFields(Long nodeFormId){
		if(nodeFormId==null){
			log.error("nodeFormId=null");
		}
		INodeFormFieldService nodeFormFieldService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
		Method[] methods=NodeEntity.class.getMethods();
		Integer ord=0;
		for(Method m:methods){
			String name=m.getName();
			String type=m.getReturnType().getSimpleName();
			if(name.substring(0,3).equals("get")){
				name=name.replace("get", "");
				if(!checkFieldIsEditable(name))
					continue;
				name=name.substring(0,1).toLowerCase()+name.substring(1);
				if(name.equals("class"))
					continue;
				log.debug(name+"\t"+type);
				NodeFormFieldEntity field=new NodeFormFieldEntity();
				field.setAssistTags(getAssistTags(name));
				field.setCreateTime(new Date());
				field.setDefaultValue(getDefaultValue(name,type));
				field.setDisplayByAddMode(true);
				field.setDisplayByEditMode(true); 
				field.setDisplayName(name);
				field.setExpGroup("");
				field.setGroup(getNodeFormFieldGroup(name));
				field.setFieldDescription("");
				field.setFieldName(name);
				field.setNodeFormId(nodeFormId);
				field.setOptionalValue(getOptionalValue(name,type));
				field.setOrder(ord++);
				field.setTagType(getTagType(name,type));
				field.setUpdateTime(new Date());
				field.setWriteByAddMode(true);
				field.setWriteByEditMode(true);
				field.setIsEditable(true);
				nodeFormFieldService.saveEntity(field);
			}
		}
		
	}
	private Boolean checkFieldIsEditable(String name){
		name=name.substring(0,1).toLowerCase()+name.substring(1);
		List<String> fieldList=new ArrayList<String>();
		fieldList.add("id");
		fieldList.add("leaf");
		fieldList.add("state");
		fieldList.add("createUser");
		fieldList.add("depth");
		fieldList.add("childCount");
		fieldList.add("countentCount");
		fieldList.add("isDeleted");
		fieldList.add("parentId");
		fieldList.add("nodeIdPath");
		fieldList.add("subSiteId");
		fieldList.add("createDate");
		fieldList.add("commentCount");
		fieldList.add("lastUpdateDate");
		fieldList.add("nodeId");
		fieldList.add("passedTime");
		fieldList.add("passedUser");
		fieldList.add("state");
		fieldList.add("displayInNavigator");
		fieldList.add("orderByNavigator");
//		for(Integer i=1;i<=20;i++){
//			fieldList.add("custom"+i);
//		}
		if(fieldList.contains(name)){//如果包含了这个字段，说明这个字段是 不可编辑的，就返回false
			return false;
		}else{
			return true;
		}
	}
	private String getNodeFormFieldGroup(String fieldName){
		fieldName=fieldName.toLowerCase();
		List<String> baseList=new ArrayList<String>();
		List<String> viewList=new ArrayList<String>();
		List<String> tempList=new ArrayList<String>();
		baseList.add(("state").toLowerCase());
		viewList.add(("Map").toLowerCase());

		baseList.add(("Depth").toLowerCase());
		baseList.add(("ParentId").toLowerCase());
		baseList.add(("NodeType").toLowerCase());
		baseList.add(("SubSiteId").toLowerCase());
		baseList.add(("Group").toLowerCase());
		baseList.add(("LinkUrl").toLowerCase());
		baseList.add(("Keywords").toLowerCase());
		baseList.add(("Text").toLowerCase());
		baseList.add(("Leaf").toLowerCase());
		baseList.add(("Order").toLowerCase());
		viewList.add(("Banner").toLowerCase());
		baseList.add(("IsDeleted").toLowerCase());
		viewList.add(("BgMusic").toLowerCase());
		baseList.add(("Copyright").toLowerCase());
		viewList.add(("Logo").toLowerCase());
		viewList.add(("Icon").toLowerCase());
		baseList.add(("ChildCount").toLowerCase());
		baseList.add(("ContentMediaId").toLowerCase());
		baseList.add(("NodeIdPath").toLowerCase());
		baseList.add(("CountentCount").toLowerCase());
		baseList.add(("Description").toLowerCase());
		viewList.add(("DisplayInNavigator").toLowerCase());
		tempList.add(("TemplateGroupId").toLowerCase());
		tempList.add(("NodePageTemplateId").toLowerCase());
		baseList.add(("SubContentCountUnit").toLowerCase());
		baseList.add(("MakeStaticPage").toLowerCase());
		tempList.add(("ContentpageTemplateId").toLowerCase());
		baseList.add(("ManageUser").toLowerCase());
		tempList.add(("CommentStyleTemplateId").toLowerCase());
		baseList.add(("CreateDate").toLowerCase());
		tempList.add(("NodeStyleTemplateId").toLowerCase());
		tempList.add(("ContentStyleTemplateId").toLowerCase());
		baseList.add(("CreateUser").toLowerCase());
		tempList.add(("CommentpageTemplateId").toLowerCase());
		baseList.add(("NodeMediaId").toLowerCase());
		baseList.add(("OrderByNavigator").toLowerCase());
		if(baseList.contains(fieldName))
			return "base";
		else if(viewList.contains(fieldName)){
			return "view";
		}else if(tempList.contains(fieldName)){
			return "template";
		}else{
			return "public";
		}
	}
	/**
	 * 该函数仅仅用来取消node-nodeForm之间的关联，该函数还有一个重载
	 */
	public void UnLinkNodeForm(Long nodeId, Long nodeFormId) {
		INodeNodeFormService nns=(INodeNodeFormService) ServiceProxyFactory.getServiceProxy(NodeNodeFormServiceImpl.class);
		List<NodeNodeFormEntity> nList=nns.getList("from "+nns.getTableName()+" where nodeId="+nodeId+" and formId="+nodeFormId, null);
		if(nList!=null && nList.size()>0){
			for(NodeNodeFormEntity n:nList){
				nns.deleteByPK(n.getId());
			}
		}
		
	}
	private void deleteNodeFormByNodeIdAndType(Long nodeId,Integer type){
		List<NodeFormEntity> nList=this.getList(getFromStr()+" where nodeId="+nodeId + " and formType="+type,null);
		if(nList!=null){
			for(NodeFormEntity n:nList){
				this.deleteByPK(n.getId());
			}
		}
	}
	/**
	 * 改栏目用来同时删除node自己建立的form和node-nodeform对照
	 */
	public void UnLinkNodeForm(Long nodeId,Integer type) {
		//先检查有没有在nodeForm中nodeId为参数nodeId值的记录，如果有，则说明当前栏目引用的是自己新建的表单
		deleteNodeFormByNodeIdAndType(nodeId,type);
		INodeNodeFormService nns=(INodeNodeFormService) ServiceProxyFactory.getServiceProxy(NodeNodeFormServiceImpl.class);
		List<NodeNodeFormEntity> nList=nns.getList("from "+nns.getTableName()+" where nodeId="+nodeId, null);
		if(nList!=null && nList.size()>0){
			for(NodeNodeFormEntity n:nList){
				if(n.getFormId().equals(-1L)){
					nns.deleteByPK(n.getId());
				}
				NodeFormEntity nf=this.getEntityById(n.getFormId());
				if(nf!=null){
					if(nf.getFormType().equals(type)){
						nns.deleteByPK(n.getId());			
					}
				}
			}
		}
	}

	public NodeFormExtEntity getDefaultNodeFormExtEntity(Integer formType) {
		NodeFormEntity nodeForm=getDefaultNodeForm(formType);
		if(nodeForm==null)
			return null;
		INodeFormFieldService nffService=(INodeFormFieldService) ServiceProxyFactory.getServiceProxy(NodeFormFieldServiceImpl.class);
		List<NodeFormFieldEntity> formFieldList=nffService.getList("from "+nffService.getTableName()+" where nodeFormId="+nodeForm.getId(),null,"order asc",1000,1);
		NodeFormExtEntity extForm=new NodeFormExtEntity(nodeForm, formFieldList);
		return extForm;
	}
}
