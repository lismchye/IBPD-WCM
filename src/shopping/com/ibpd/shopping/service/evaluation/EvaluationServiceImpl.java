package com.ibpd.shopping.service.evaluation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.EvaluationEntity;
@Service("evaluationService")
public class EvaluationServiceImpl extends BaseServiceImpl<EvaluationEntity> implements IEvaluationService {
	public EvaluationServiceImpl(){
		super();
		this.tableName="EvaluationEntity";
		this.currentClass=EvaluationEntity.class;
		this.initOK();
	}

	public List<EvaluationEntity> getListByAccountId(Long accId,Long productId,Integer pageSize,Integer pageIndex) {
		return getList("from "+this.getTableName()+" where accId="+accId+" and productId="+productId,null,"createDate desc",pageSize,pageIndex);
	}

}
