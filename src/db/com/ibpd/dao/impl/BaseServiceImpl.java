 package com.ibpd.dao.impl;
 
 import com.ibpd.common.PageCommon;
 import com.ibpd.dao.IDao;
 import com.ibpd.entity.baseEntity.IBaseEntity;
 import java.io.Serializable;
 import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

 public class BaseServiceImpl<Entity extends IBaseEntity>
   implements IBaseService<Entity>
 {
/*  14 */   private Logger log = Logger.getLogger(BaseServiceImpl.class);
/*  15 */   public String tableName = "";
			@Resource(name="daoImpl")
   private IDao<Entity> dao;
/*  17 */   public Class currentClass = IBaseEntity.class;
 
   public void initOK()
   {
/*  21 */     this.log.info("==========初始化service..." + this.tableName + "|" + this.currentClass + " OK");
   }
 
   public Boolean batchDelete(Long[] ids)
   {
/*  27 */     for (int i = 0; i < ids.length; ++i) {
       try {
/*  29 */         this.dao.delete(this.currentClass, ids[i]);
       } catch (Exception ex) {
/*  31 */         ex.printStackTrace();
/*  32 */         return Boolean.valueOf(false);
       }
     }
/*  35 */     return Boolean.valueOf(true);
   }
 
   public int deleteByPK(Serializable pk)
   {
/*  40 */     this.dao.delete(this.currentClass, pk);
/*  41 */     return PageCommon.CODE_OK.intValue();
   }
 
   public IDao<Entity> getDao()
   {
/*  46 */     return this.dao;
   }
 
   public Entity getEntityById(Long id)
   {
/*  51 */     return this.dao.getEntity(this.currentClass, id);
   }
 
   public List<Entity> getList()
   {
/*  56 */     return this.dao.getEntityList("from " + this.tableName, null,null, Integer.valueOf(-1), Integer.valueOf(-1));
   }
 
   public List<Entity> getList(String jpql,List<HqlParameter> hqlParameterList, String orderType, Integer pageSize, Integer pageIndex)
   {
/*  62 */     return this.dao.getEntityList(jpql, hqlParameterList, orderType, pageSize, pageIndex);
   }
 
   public List<Entity> getList(String orderType, Integer pageSize, Integer pageIndex) {
/*  66 */     return this.dao.getEntityList("from " + this.tableName,null, orderType, pageSize, pageIndex);
   }
 
   public Long getRowCount(String whereString,List<HqlParameter> hqlParameterList)
   {
/*  71 */     return this.dao.getItemCount(this.tableName, whereString,hqlParameterList);
   }
 
   public String getTableName()
   {
/*  76 */     return this.tableName;
   }
 
   public int saveEntity(Entity entity)
   {
/*  81 */     this.dao.save(entity);
/*  82 */     return PageCommon.CODE_OK.intValue();
   }
 
   public void setDao(IDao<Entity> dao)
   {
/*  87 */     this.dao = dao;
   }
 
   public int updateEntity(Entity entity)
   {
/*  92 */     this.dao.update(entity);
/*  93 */     return PageCommon.CODE_OK.intValue();
   }
 
   public int batchDel(String ids)
   {
     try {
/*  99 */       if (ids != null) {
/* 100 */         String[] subIds = ids.split(",");
/* 101 */         String jpql = " where ";
/* 102 */         for (Integer i = Integer.valueOf(0); i.intValue() < subIds.length; i = Integer.valueOf(i.intValue() + 1)) {
/* 103 */           jpql = jpql + "id=" + subIds[i.intValue()] + " or ";
         }
/* 105 */         if (jpql.length() > " where ".length())
/* 106 */           jpql = jpql.substring(0, jpql.length() - 4);
         else
/* 108 */           jpql = "id=-99";
/* 109 */         getDao().saveOrUpdate("delete from " + getTableName() + jpql, this.currentClass);
/* 110 */         return PageCommon.CODE_OK.intValue();
       }
/* 112 */       return PageCommon.CODE_OK.intValue();
     }
     catch (Exception ex) {
/* 115 */       ex.printStackTrace();
/* 116 */     }return PageCommon.CODE_ERR.intValue();
   }
 
   public List<Entity> getList(String jpql,List<HqlParameter> hqlParameterList) {
/* 120 */     return getList(jpql,hqlParameterList, null, Integer.valueOf(-1), Integer.valueOf(-1));
   }
 }