package com.ibpd.shopping.web.controller.manage;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.TreeNodeEntity;
import com.ibpd.henuocms.entity.ext.TreeNodeUtil;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.CatalogEntity;
import com.ibpd.shopping.entity.ProductAttrDefineEntity;
import com.ibpd.shopping.service.catalog.CatalogServiceImpl;
import com.ibpd.shopping.service.catalog.ICatalogService;
import com.ibpd.shopping.service.product.IProductAttrDefineService;
import com.ibpd.shopping.service.product.ProductAttrDefineServiceImpl;
@Controller
public class Catalog extends BaseController {
	@RequestMapping("Manage/Catalog/index.do")
	public String index(String id,Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","商品分类");
		model.addAttribute("cataId",id);
		model.addAttribute("loginedUserName",SysUtil.getCurrentLoginedUserName(req.getSession()));
		model.addAttribute("loginedUserType",SysUtil.getCurrentLoginedUserType(req.getSession()));
		return "manage/s_catalog/index";
	}
	@RequestMapping("Manage/Catalog/tree.do")
	public void tree(Long id,HttpServletRequest req,HttpServletResponse resp) throws IOException{
		String type=SysUtil.getCurrentLoginedUserType(req.getSession());
		String productManage="{\"id\":\"-1\",\"text\":\"商品管理\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String orderManage="{\"id\":\"-2\",\"text\":\"订单管理\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String accountManage="{\"id\":\"-3\",\"text\":\"会员管理\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String tenantManage="{\"id\":\"-4\",\"text\":\"商户管理\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String expManage="{\"id\":\"-6\",\"text\":\"快递方式\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String sellManage="{\"id\":\"-5\",\"text\":\"销售分析\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		String appManage="{\"id\":\"-7\",\"text\":\"商城首页\",\"iconCls\":\"icon-save\",\"state\":\"closed\"}";
		Long nodeId=null;
		if(id==null){
			nodeId=null;
		}else{
			nodeId=id;
		}
		//
		String rtnJson="";
		if(nodeId==null){
			rtnJson+="[{\"id\":\"0\",\"text\":\"商品分类\",\"iconCls\":\"icon-save\",\"state\":\"closed\"},";
			rtnJson+=expManage+",";//让商户也能管理自己的配送方式
			rtnJson+=orderManage+",";//让商户也能管理自己的配送方式
//			rtnJson+=productManage+",";
			if(type=="manage")
//				rtnJson+=orderManage+",";
			if(type=="manage")
				rtnJson+=accountManage+",";
			if(type=="manage")
				rtnJson+=tenantManage+",";
//			if(type=="manage")
//				rtnJson+=expManage+",";
			if(type=="manage")
				rtnJson+=sellManage+",";
			if(type=="manage"){
				rtnJson+=appManage;
			} 
			if(type!="manage")
				rtnJson=rtnJson.substring(0,rtnJson.length()-1);
			rtnJson+="]";
		}else if(nodeId==-1L){
			//商品管理 
		}else if(nodeId==-2L){
			//订单管理
			rtnJson="[{\"id\":\"-21\",\"text\":\"已下单未审核订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-22\",\"text\":\"已审核未付款订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-23\",\"text\":\"已付款未发货订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-24\",\"text\":\"已发货未签收订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-25\",\"text\":\"已签收未归档订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-26\",\"text\":\"已归档订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-27\",\"text\":\"已取消订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-28\",\"text\":\"退款退货订单\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+="]";
		}else if(nodeId==-3L){
			//会员管理
		}else if(nodeId==-4L){
			//商户管理
			rtnJson="[{\"id\":\"-41\",\"text\":\"企业商户管理\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-42\",\"text\":\"代理商户管理\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+="]";
		}else if(nodeId==-5L){
			//销售分析
		}else if(nodeId==-7L){
			//商户管理
			rtnJson="[{\"id\":\"-71\",\"text\":\"幻灯管理\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-72\",\"text\":\"今日推荐\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-73\",\"text\":\"热门市场\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+=",{\"id\":\"-74\",\"text\":\"会员专卖\",\"iconCls\":\"icon-file\",\"state\":\"open\"}";
			rtnJson+="]";
		}else{
			
			ICatalogService cataServ=(ICatalogService) getService(CatalogServiceImpl.class);
			List<HqlParameter> pList=new ArrayList<HqlParameter>();
			pList.add(new HqlParameter("pid",nodeId,null));
			List<CatalogEntity> cataList=cataServ.getList("from "+cataServ.getTableName()+" where pid=:pid",pList,"order asc",-1,-1);
			List<NodeEntity> nodeList=convertToNodeList(cataList);
			List<TreeNodeEntity> treeNodeList=TreeNodeUtil.converToTreeNode(nodeList);
			JSONArray jsonArray = JSONArray.fromObject( treeNodeList );
			rtnJson=jsonArray.toString();
		}
		PrintWriter out = resp.getWriter();
		 out.print(rtnJson);
		 out.flush();
	}
	@RequestMapping("Manage/Catalog/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		if(StringUtil.isBlank(ids)){
			super.printMsg(resp, "-1", "", "没有选择项");	
		}
		ICatalogService cataServ=(ICatalogService) getService(CatalogServiceImpl.class);
		String[] is=ids.split(",");
		String  whereString="from "+cataServ.getTableName()+" where ";
		for(String id:is){
			whereString+="id="+id+" or ";
		}
		whereString=whereString.substring(0,whereString.length()-4);
		List<CatalogEntity> ceList=cataServ.getList(whereString, null);
		//本来计划这里将所有cata的父cata取出来操作的，原因是因为某些特殊情况，但这里就不这么做了，除非有人恶意操作，但即使这样，顶多是操作失败
		Map<Long,Long> pidMap=new HashMap<Long,Long>();
		for(CatalogEntity c:ceList){
			pidMap.put(c.getPid(), c.getPid());
		}
		cataServ.batchDel(ids);
		whereString="from "+cataServ.getTableName()+" where ";
		for(Long key:pidMap.keySet()){
			whereString+="id="+key+" or ";
		}
		whereString=whereString.substring(0,whereString.length()-4);
		List<CatalogEntity> pceList=cataServ.getList(whereString,null);
		if(pceList!=null && pceList.size()>0){
			for(CatalogEntity pc:pceList){
				List<CatalogEntity> tmpList=cataServ.getList("from "+cataServ.getTableName()+" where pid="+pc.getId(),null);
				if(tmpList==null || tmpList.size()==0){ 
					pc.setLeaf(false);  
					cataServ.saveEntity(pc);	 				
				}
			} 
		}
		super.printMsg(resp, "99", "", "操作成功");	
	}
	@RequestMapping("Manage/Catalog/list.do")
	public void list(String id,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		ICatalogService cataServ=(ICatalogService) getService(CatalogServiceImpl.class);
		String query=" pid="+id;
//		cataServ.getDao().clearCache();
		super.getList(req, cataServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Catalog/props.do")
	public String props(Long id,Model model,HttpServletRequest req) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null || id<0L){
			model.addAttribute("errorMsg","参数错误");
			return super.ERROR_PAGE;
		}
		ICatalogService cataServ=(ICatalogService) getService(CatalogServiceImpl.class);
		CatalogEntity ce=cataServ.getEntityById(id);
		if(ce==null){
			model.addAttribute("errorMsg","没有该类别");
			return super.ERROR_PAGE;
		}//loginedUserType
		model.addAttribute("pageTitle","参数设置");
		model.addAttribute("htmls",super.getHtmlString(ce, "catalog.edit.field"));
		model.addAttribute("entity",ce);
		model.addAttribute("loginedUserType",SysUtil.getCurrentLoginedUserType(req.getSession()));
		return "manage/s_catalog/props";
	}	
	@RequestMapping("Manage/Catalog/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
				ICatalogService cataServ=(ICatalogService) ServiceProxyFactory.getServiceProxy(CatalogServiceImpl.class);
				CatalogEntity cata=cataServ.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(Account.class).info("value="+val);
					method.invoke(cata, val);
					cataServ.saveEntity(cata);
//					cataServ.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");			
		}
	}
		@RequestMapping("Manage/Catalog/navi.do")
		public void navi(Long id,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id!=null && id!=-1){
				ICatalogService cataServ=(ICatalogService) ServiceProxyFactory.getServiceProxy(CatalogServiceImpl.class);
				CatalogEntity cata=cataServ.getEntityById(id);
				if(cata!=null){
					cata.setShowInNav(cata.getShowInNav().trim().toLowerCase().equals("n")?"y":"n");
					cataServ.saveEntity(cata);
				}
			}
		
	}
		@RequestMapping("Manage/Catalog/toAdd.do")
		public String toAdd(String parentId,Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(StringUtils.isEmpty(parentId)){
				return this.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","添加分类");
			model.addAttribute("parentId",parentId);
			model.addAttribute("htmls",super.getHtmlString(null, "catalog.add.field"));
			return "manage/s_catalog/add";
		}
		@RequestMapping("Manage/Catalog/toEdit.do")
		public String toEdit(Long id,Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null){
				return this.ERROR_PAGE;
			}
			ICatalogService cataServ=(ICatalogService) ServiceProxyFactory.getServiceProxy(CatalogServiceImpl.class);
			CatalogEntity cata=cataServ.getEntityById(id);
			if(cata==null){
				model.addAttribute("errorMsg","没有该类别");
				return this.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","添加分类");
			model.addAttribute("htmls",super.getHtmlString(cata, "catalog.edit.field"));
			model.addAttribute("entity",cata);
			return "manage/s_catalog/edit";
		}
		@RequestMapping("Manage/Catalog/doEdit.do")
		public void doEdit(CatalogEntity entity,HttpServletResponse resp) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(entity!=null && entity.getId()!=null && entity.getId()!=-1L){
				
				ICatalogService cataServ=(ICatalogService) ServiceProxyFactory.getServiceProxy(CatalogServiceImpl.class);
				CatalogEntity cata=cataServ.getEntityById(entity.getId());
				if(cata==null){
					super.printMsg(resp, "-1", "", "没有该类别");
					return;
				}else{
					swap(entity,cata);
					cataServ.saveEntity(cata);
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");
					return;
				}
			}else{
				super.printMsg(resp, "-2", "", "参数错误");
				return;
			}
			
		}
		@RequestMapping("Manage/Catalog/doAdd.do")
		public void doAdd(CatalogEntity entity,HttpServletResponse resp) throws IOException{
			if(entity.getPid()==null){
				super.printMsg(resp, "-1", "-1", "parentId_is_null");
				return;
			}
			
			if(entity.getName().trim().equals("")){
				super.printMsg(resp, "-1", "-1", "参数错误");
				return;
			}
			ICatalogService cataServ=(ICatalogService) ServiceProxyFactory.getServiceProxy(CatalogServiceImpl.class);
			CatalogEntity parentNode=(entity.getPid()!=-1L)?cataServ.getEntityById(entity.getPid()):null;
			entity.setLeaf(false); 
			cataServ.saveEntity(entity);
			if(parentNode!=null){
				if(!parentNode.getLeaf()){
					parentNode.setLeaf(true);
				}				
				cataServ.saveEntity(parentNode) ;	
			}
			super.printMsg(resp, "99", "-1", "保存成功");
			 
		}
		@RequestMapping("Manage/Catalog/params.do")
		public String params(Long id ,Integer type,HttpServletResponse resp,Model model) throws IOException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","分类参数管理");
			model.addAttribute("cataId",id);
			model.addAttribute("defType",type);
			return "manage/s_catalog/params";
		}
		@RequestMapping("Manage/Catalog/attributes.do")
		public String  attributes(Long id ,Integer type,HttpServletResponse resp,Model model) throws IOException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","分类属性管理");
			model.addAttribute("cataId",id);
			model.addAttribute("defType",type);
			return "manage/s_catalog/params";
		}
		@RequestMapping("Manage/Catalog/toAddParams.do")
		public String  toAddParams(Long id ,Integer type,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","添加参数");
			model.addAttribute("cataId",id);			
			model.addAttribute("defType",type);
			model.addAttribute("htmls",super.getHtmlString(null, "catalogParams.add.field"));
			return "manage/s_catalog/addParam";
		}
		@RequestMapping("Manage/Catalog/toAddAttribute.do")
		public String  toAddAttribute(Long id ,Integer type,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			model.addAttribute("pageTitle","添加属性");
			model.addAttribute("cataId",id);			
			model.addAttribute("defType",type);
			model.addAttribute("htmls",super.getHtmlString(null, "catalogAttributes.add.field"));
			return "manage/s_catalog/addParam";
		}
		@RequestMapping("Manage/Catalog/toEditParams.do")
		public String  toEditParams(Long id ,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
//			attrServ.getDao().clearCache();
			ProductAttrDefineEntity attr=attrServ.getEntityById(id);
			model.addAttribute("pageTitle","编辑");
			model.addAttribute("cataId",id);			
			model.addAttribute("entity",attr);
			model.addAttribute("htmls",super.getHtmlString(attr, "catalogAttributes.edit.field"));
			return "manage/s_catalog/editParam";
		}
		@RequestMapping("Manage/Catalog/params_props.do")
		public String  params_props(Long id ,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			ProductAttrDefineEntity attr=attrServ.getEntityById(id);
			model.addAttribute("pageTitle","属性配置");
			model.addAttribute("cataId",id);			
			model.addAttribute("entity",attr);
			model.addAttribute("htmls",super.getHtmlString(attr, "catalogAttributes.edit.field"));
			return "manage/s_catalog/param_props";
		}
		@RequestMapping("Manage/Catalog/toEditAttribute.do")
		public String  toEditAttribute(Long id ,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(id==null || id<=0){
				model.addAttribute("errorMsg","未选择具体类别");
				return super.ERROR_PAGE;
			}
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			ProductAttrDefineEntity attr=attrServ.getEntityById(id);
			model.addAttribute("pageTitle","编辑");
			model.addAttribute("cataId",id);			
			model.addAttribute("entity",attr);
			model.addAttribute("htmls",super.getHtmlString(attr, "catalogAttributes.edit.field"));
			return "manage/s_catalog/editParam";
		}
		@RequestMapping("Manage/Catalog/doAddParam.do")
		public void  doAddParam(ProductAttrDefineEntity entity ,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(StringUtil.isBlank(entity.getAttrName())){
				super.printMsg(resp, "-1", "", "名称不能为空");
				return;
			}
			if(StringUtil.isBlank(entity.getType())){
				super.printMsg(resp, "-1", "", "类别不能为空");
				return;
			}
			if(entity.getCatalogId()==null){
				super.printMsg(resp, "-1", "", "类别ID不能为空");
				return;
			}
			if(entity.getDefClass()==null){
				super.printMsg(resp, "-1", "", "参数错误");
				return;
			}
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			attrServ.saveEntity(entity);
//			attrServ.getDao().clearCache();
			super.printMsg(resp, "99", entity.getId().toString(), "保存成功");
		}
		@RequestMapping("Manage/Catalog/doEditParam.do")
		public void  doEditParam(ProductAttrDefineEntity entity ,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
			if(entity.getId()==null || entity.getId()<=-1){
				super.printMsg(resp, "-1", "", "ID不能为空");
				return;				
			}
			if(StringUtil.isBlank(entity.getAttrName())){
				super.printMsg(resp, "-1", "", "名称不能为空");
				return;
			}
			if(StringUtil.isBlank(entity.getType())){
				super.printMsg(resp, "-1", "", "类别不能为空");
				return;
			}
			if(entity.getCatalogId()==null){
				super.printMsg(resp, "-1", "", "类别ID不能为空");
				return;
			}
			if(entity.getDefClass()==null){
				super.printMsg(resp, "-1", "", "参数错误");
				return;
			}
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			ProductAttrDefineEntity attr=attrServ.getEntityById(entity.getId());
			if(attr==null){
				super.printMsg(resp, "-1", "", "没有该实体");
				return;
			}
			super.swap(entity, attr);
			attrServ.saveEntity(attr);
//			attrServ.getDao().clearCache();
			super.printMsg(resp, "99", entity.getId().toString(), "保存成功");
		}
		@RequestMapping("Manage/Catalog/getParamsList.do")
		public void getParamsList(Long id ,Integer type,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			String query="catalogId="+id+" and defClass="+type;
//			attrServ.getDao().clearCache();
			super.getList(req, attrServ, resp, order, page, rows, sort, query);
		}
		@RequestMapping("Manage/Catalog/doDelParam.do")
		public void  doDelParam(String ids,HttpServletResponse resp,Model model) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(ids==null || StringUtil.isBlank(ids)){
				super.printMsg(resp, "-1", "", "未选择项");
				return;
			}
			
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			attrServ.batchDel(ids);
			super.printMsg(resp, "99", "", "执行成功");
		}
		
	private List<NodeEntity> convertToNodeList(List<CatalogEntity> cataList){
		if(cataList==null)return null;
		List<NodeEntity> nodeList=new ArrayList<NodeEntity>();
		for(CatalogEntity c:cataList){
			NodeEntity ne=new NodeEntity();
			ne.setId(c.getId());
			ne.setLeaf(c.getLeaf());
			ne.setParentId(c.getPid());
			ne.setText(c.getName());
			nodeList.add(ne);
		}
		return nodeList;
	}
	@RequestMapping("Manage/Catalog/saveParamProps.do")
	public void saveParamProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
			IProductAttrDefineService attrServ=(IProductAttrDefineService)getService(ProductAttrDefineServiceImpl.class);
			ProductAttrDefineEntity cata=attrServ.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(Account.class).info("value="+val);
					method.invoke(cata, val);
					attrServ.saveEntity(cata);
//					attrServ.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");			
		}
	}
}
