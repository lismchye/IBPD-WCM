package com.ibpd.henuocms.service.pageTemplate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hsqldb.lib.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.TemplateUtil;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.ext.PageTemplateExtEntity;
import com.ibpd.henuocms.exception.IbpdException;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
@Transactional
@Service("pageTemplateService")
public class PageTemplateServiceImpl extends BaseServiceImpl<PageTemplateEntity> implements IPageTemplateService {
	public PageTemplateServiceImpl(){
		super();
		this.tableName="PageTemplateEntity";
		this.currentClass=PageTemplateEntity.class;
		this.initOK();
	}
 
	@Override
	public int saveEntity(PageTemplateEntity entity) {
		if(StringUtils.isEmpty(entity.getTemplateFilePath())){
			entity.setTemplateFilePath(TemplateUtil.makeNewPageTemplate(entity.getType()));
		}else{
			String path=IbpdCommon.getInterface().getPageTemplateRealDir(entity.getId());
			File f=new File(path);
			if(!f.exists()){
				entity.setTemplateFilePath(TemplateUtil.makeNewPageTemplate(entity.getType()));
			}
		}
		
		entity.setFolderSize(TemplateUtil.getDirSize(entity.getTemplateFilePath()));
		entity.setSize(TemplateUtil.getFileSize(entity.getTemplateFilePath()));
		return super.saveEntity(entity);
	}

	public void init() {
		List<PageTemplateEntity> pList=this.getList();
		for(PageTemplateEntity p:pList){
			this.deleteByPK(p.getId());
		}
		IStyleTemplateService styleService=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		List<StyleTemplateEntity> sList=styleService.getList();
		for(StyleTemplateEntity s:sList){
			styleService.deleteByPK(s.getId());
		}
		StyleTemplateEntity st=getStyleTemplate("默认站点样式模版",StyleTemplateEntity.TEMPLATE_TYPE_SITE);
		styleService.saveEntity(st);
		PageTemplateEntity pt= getPageTemplate(st.getId(),"默认站点页面模版",PageTemplateEntity.TEMPLATE_TYPE_SITE);
		this.saveEntity(pt);
		StyleTemplateEntity st2=getStyleTemplate("默认栏目样式模版",StyleTemplateEntity.TEMPLATE_TYPE_NODE);
		styleService.saveEntity(st2);
		PageTemplateEntity pt2= getPageTemplate(st2.getId(),"默认栏目页面模版",PageTemplateEntity.TEMPLATE_TYPE_NODE);
		this.saveEntity(pt2);
		StyleTemplateEntity st3=getStyleTemplate("默认内容样式模版",StyleTemplateEntity.TEMPLATE_TYPE_CONTENT);
		styleService.saveEntity(st3);
		PageTemplateEntity pt3= getPageTemplate(st2.getId(),"默认内容页面模版",PageTemplateEntity.TEMPLATE_TYPE_CONTENT);
		this.saveEntity(pt3);
	}
	private StyleTemplateEntity getStyleTemplate(String title,Integer type){
		StyleTemplateEntity st=new StyleTemplateEntity();
		st.setSubSiteId(-1L);
		st.setTitle(title);
		st.setType(type);
		return st;
	}
	private PageTemplateEntity getPageTemplate(Long styleId,String title,Integer type){
		PageTemplateEntity pt=new PageTemplateEntity();
		pt.setDefaultStyleTemplateId(styleId);
		pt.setIsDefault(true);
		pt.setTitle(title);
		pt.setType(type);
		return pt;
	}

	public List<PageTemplateExtEntity> getList(Long[] siteIds, Long NodeId,
			Integer pageSize, Integer pageIhndex, 
			String orderType, String filterString) {
		List<PageTemplateExtEntity> tempExtList=new ArrayList<PageTemplateExtEntity>();
		if(NodeId!=null){
			
			INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
			NodeEntity node=nodeService.getEntityById(NodeId);
			if(node==null)
				return null;
			String idPath=node.getNodeIdPath();
			String ids[]=idPath.split(",");
			String hql="from "+this.getTableName()+" where ";
			for(String id:ids){
				if(!StringUtil.isEmpty(id))
					hql+="nodeId="+id+" or ";
			}
			if(hql.endsWith(" or ")){
				hql=hql.substring(0, hql.length()-4);
				hql=hql+((filterString==null)?"":" and "+filterString);
				search(hql,tempExtList,pageSize,pageIhndex,orderType);
			}
			return tempExtList;
		}else if(siteIds!=null && siteIds.length>0){
			
			Long ids[]=siteIds;
			String hql="from "+this.getTableName()+" where ";
			for(Long id:ids){
				if(id!=null)
					hql+="subSiteId="+id.toString()+" or ";
			} 
			if(hql.endsWith(" or ")){
				hql=hql.substring(0, hql.length()-4);
				hql=(filterString==null)?hql+"":hql+" and "+filterString;
				search(hql,tempExtList,pageSize,pageIhndex,orderType);
			}
			return tempExtList;
		}else
			return null;
	}
	private void search(String hql,List<PageTemplateExtEntity> tempExtList,Integer pageSize,Integer pageIndex,String order){
		List<PageTemplateEntity> pageList=this.getList(hql, null, order, pageSize, (pageIndex==0)?0:pageIndex-1);
		
		IStyleTemplateService styleService=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		for(PageTemplateEntity p:pageList){
			PageTemplateExtEntity ext=new PageTemplateExtEntity();
			StyleTemplateEntity style=styleService.getEntityById(p.getDefaultStyleTemplateId());
			ext.setStyleTemplate(style);
			ext.setPageTemplate(p);
			tempExtList.add(ext);
		}

	}
	public Long getRowCount(Long[] siteIds, Long nodeId, String filterString) {
		if(nodeId!=null){
			INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
			NodeEntity node=nodeService.getEntityById(nodeId);
			if(node==null)
				return 0L;
			String idPath=node.getNodeIdPath();
			String ids[]=idPath.split(",");
			String hql=" where ";
			for(String id:ids){
				if(!StringUtil.isEmpty(id))
					hql+="nodeId="+id+" or ";
			}
			if(hql.equals(" where ")){
				hql="";
			}
			if(hql.endsWith(" or ")){
				hql=hql.substring(0, hql.length()-4);
				 
			}
			return this.getRowCount(hql, null);
		}else if(siteIds!=null && siteIds.length>0){
			Long ids[]=siteIds;
			String hql=" where "; 
			for(Long id:ids){ 
				if(id!=null)
					hql+="subSiteId="+id.toString()+" or ";
			} 
			if(hql.endsWith(" or ")){
				hql=hql.substring(0, hql.length()-4);
			}
			return this.getRowCount(hql, null);
		}else
			return null;
	}

	public Boolean checkPageTempIsUsed(Long id) {
		if(id!=null && id!=-1L){
			ISubSiteService siteService=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
			List l=siteService.getList("from "+siteService.getTableName()+" where sitePageTemplateId="+id+" or nodePageTemplateId="+id+" or contentpageTemplateId="+id+" or commentpageTemplateId="+id, null);
			if(l!=null && l.size()>0){
				return true;
			}else{
				INodeAttrService nodeService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
				List n=nodeService.getList("from "+nodeService.getTableName()+" where nodePageTemplateId="+id+" or contentpageTemplateId="+id+" or commentpageTemplateId="+id, null);
				if(n!=null && n.size()>0){
					return true;
				}else{
					return false;
				}
			}
		}else{
			try {
				throw new IbpdException("ID为null 或  -1");
			} catch (IbpdException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
	}

	public PageTemplateEntity getDefaultPageTemplate(Integer type) {
		List<PageTemplateEntity> pageTemplateList=this.getList("from "+this.getTableName()+" where type="+type+" and isDefault=true", null);
		if(pageTemplateList!=null && pageTemplateList.size()>0)
			return pageTemplateList.get(0);
		else
			return null;
	}

	public void setStyleTempateId(Long pageTempId,Long styleTempId) {
		if(pageTempId==null || styleTempId==null)
			return;
		PageTemplateEntity pt=this.getEntityById(pageTempId);
		pt.setDefaultStyleTemplateId(styleTempId);
		this.saveEntity(pt);
	}
}
