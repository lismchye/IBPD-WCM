<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:80px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr td input {
			width:150px;
		}
		.baseInfo tr td select {
			width:150px;
		}
		</style> 
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script src="<%=basePath%>js/ckeditor/ckeditor.js"></script>
	
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   
    <div data-options="region:'center'"  id="center" region="center" style="background: #eee; overflow-y:hidden">
   	 <textarea id="text">${text }</textarea>
    </div>
   <div data-options="region:'east'" region="east" split="false" title="属性配置" style="width:300px;" id="west">
						<c:if test="${type=='add' }">
							<input mthod="smt" name="nodeId" id="s_nodeId" type="hidden" value="${nodeId }"/>
						</c:if>
						<c:if test="${type=='edit' }">
							<input mthod="smt" name="nodeId" id="s_nodeId" type="hidden" value="${entity.nodeId }"/>
							<input mthod="smt" name="id" id="s_id" type="hidden" value="${entity.id }"/>
						</c:if>
		<table class="baseInfo" cellpadding="0" cellspacing="0">
			
			<tr> 
				<td class="tit">title</td>
				<td class="val">
				<span>
					<span style="width:120px;">
						<input type="text" mthod="smt" style="width:120px;" class="easyui-textbox" id="s_title" name="title" value="${entity.title }"/>
						<input name="titleFormatParams" id="s_titleFormatParams" type="hidden" value="${titleFormatParameters }"/>
					</span>
					<span style="width:20px;">
						<input type="button" style="width:20px;" value="..." id="title_btn">
					</span>
				</span>
					
				</td>
			</tr>
			<tr>
				<td class="tit">previewUrl</td>
				<td class="val">
				<span style="width:120px;">
					<input type="text" mthod="smt" name="previewUrl" style="width:120px;" value="${entity.previewUrl }"/>
				</span>
					<span style="width:20px;">
						<input type="button" style="width:20px;" value="..." iname="previewUrl" itype="fileSelect">
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit">description</td>
				<td class="val">
					<input type="text" mthod="smt" name="description" value="${entity.description }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">textKeywords</td>
				<td class="val">
					<input type="text" mthod="smt" name="textKeywords" value="${entity.textKeywords }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">redirectUrl</td>
				<td class="val">
					<input type="text" mthod="smt" name="redirectUrl" value="${entity.redirectUrl }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">aboutKeyword</td>
				<td class="val">
					<input type="text" mthod="smt" name="aboutKeyword" value="${entity.aboutKeyword }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">isRecommend</td>
				<td class="val">
					<select mthod="smt" name="isRecommend" value="${entity.isRecommend }">
						<option value="true" <c:if test="${entity.isRecommend==true }">selected</c:if>>是</option>
						<option value="false" <c:if test="${entity.isRecommend==false }">selected</c:if>>否</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class="tit">group</td>
				<td class="val">
					<input type="text" mthod="smt" name="group" value="${entity.group }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">source</td>
				<td class="val">
					<input type="text" mthod="smt" name="source" value="${entity.source }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">url</td>
				<td class="val">
					<input type="text" mthod="smt" name="url" value="${entity.url }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">author</td>
				<td class="val">
					<input type="text" mthod="smt" name="author" value="${entity.author }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">isTop</td>
				<td class="val">
					<select mthod="smt" name="isTop" value="${entity.isTop }">
						<option value="true" <c:if test="${entity.isTop==true }">selected</c:if>>是</option>
						<option value="false" <c:if test="${entity.isTop==false }">selected</c:if>>否</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">isParson</td>
				<td class="val">
					<select mthod="smt" name="isParson" value="${entity.isParson }">
						<option value="true" <c:if test="${entity.isParson==true }">selected</c:if>>是</option>
						<option value="false" <c:if test="${entity.isParson==false }">selected</c:if>>否</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class="tit">goodCount</td>
				<td class="val">
					<input type="text" mthod="smt" name="goodCount" value="${entity.goodCount }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom1</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom1" value="${entity.custom1 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom2</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom2" value="${entity.custom2 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom3</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom3" value="${entity.custom3 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom4</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom4" value="${entity.custom4 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom5</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom5" value="${entity.custom5 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom6</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom6" value="${entity.custom6 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom7</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom7" value="${entity.custom7 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom8</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom8" value="${entity.custom8 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom9</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom9" value="${entity.custom9 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom10</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom10" value="${entity.custom10 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom11</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom11" value="${entity.custom11 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom12</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom12" value="${entity.custom12 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom13</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom13" value="${entity.custom13 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom14</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom14" value="${entity.custom14 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom15</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom15" value="${entity.custom15 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom16</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom16" value="${entity.custom16 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom17</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom17" value="${entity.custom17 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom18</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom18" value="${entity.custom18 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom19</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom19" value="${entity.custom19 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom20</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom20" value="${entity.custom20 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">复制到</td>
				<td class="val">
					<input class="easyui-combotree" id="copyTo" value="${copyToNode}">
				</td>
			</tr>
			<tr>
				<td class="tit">分发到</td> 
				<td class="val">  
					
					<input class="easyui-combotree" id="linkTo" value="${linkToNode }">
				</td>
			</tr>
		</table>
	    
    </div>
    <div id="top" data-options="region:'north'" region="north" style="background: #eee; overflow-y:hidden">
    <div class="datagrid-toolbar">
    	<table cellspacing="0" cellpadding="0">
    		<tbody>
    			<tr>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="save">
    					<span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">保存</span>
    						<span class="l-btn-icon icon-add">&nbsp;</span>
    					</span>
    					</a>
    				</td>
     				<td>
    					
    				<br></td>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="close"><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">关闭</span><span class="l-btn-icon icon-remove">&nbsp;</span></span></a>
    				</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
    </div>
	</div>

	<script type="text/javascript">
	var path="<%=path %>";
	var textEditor=null;
	var process=null;
	var type="${type }";
	var nodeId="${nodeId }";
	if(type=='edit'){
		nodeId="${entity.nodeId }";
	}
	var copyToId=getsp("${copyTo }");
	var linkToId=getsp("${linkTo }");
	function getsp(s){
	return s.split(":");
	};
	$(document).ready(function(){
		textEditor=CKEDITOR.replace('text');
		
		//textEditor.on("instanceReady",function(){textEditor.setData("${text }");});
	
		setTimeout(function(){$("#cke_1_contents").height($("#cke_1_contents").height()+$("#center").height()-$("#cke_text").height());},500);
		$("#title_btn").bind("click",{s:this},formatTitle);
		$("#save").click(function(){save();});
		$("#close").click(function(){close();});
		$("input[itype='fileSelect']").click(function(e){selectFile(e);});
		$("#copyTo").combotree({url:path+'/Manage/Node/tree.do',method:'post',multiple:true,onlyLeafCheck:true,onLoadSuccess:function(){$("#copyTo").combotree("setValues",copyToId)}});
		$("#linkTo").combotree({url:path+'/Manage/Node/tree.do',method:'post',multiple:true,onlyLeafCheck:true,onLoadSuccess:function(){$("#linkTo").combotree("setValues",linkToId)}});
		if(type=='edit'){
			$("#copyTo").combotree({disabled:true});
			$("#linkTo").combotree({disabled:true});
		}
	}); 
	function selectFile(e){
		var filter=$(e.target).attr("iname");
		var ifm=$("<div id='selectFileDialog'/>").appendTo('body');
		$(ifm).dialog({
        	modal:true,
        	title:'选择文件',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:820,
        	height:620,
        	resizable:true,
        	toolbar:[{
                    text:'关闭窗口',
                    iconCls:'icon-save',
                    handler:function(){
                    	$(ifm).dialog("close");
						$(ifm).remove();
                    }
                }], 
        	content:"<iframe id=\"ifm\" src='"+path+"/Manage/FileSelecter/index.do?nodeId="+nodeId+"&filter="+filter+"' frameborder='0' width='800px' height='550px'></iframe>"
        }); 
        $(ifm).dialog("open");
	};
	function changeFile(s,f){
	if(s!=""){
		s=s.substring(0,s.length-1);
	}
	$("input[name='"+f+"']").val(s);
	$("#selectFileDialog").dialog("close");
	$("#selectFileDialog").remove();
	};
	function save(){
	
		var ss=$("input[mthod]"); 
		var p="";  
		for(s=0;s<ss.length;s++){
			p+=""+ss[s].name+":'"+$(ss[s]).val()+"',";
		} 
		if(p.length>0){
			//p="{"+p+"'titleFormat':'"+$("#s_titleFormatParams").val()+"'}");
			p="{"+p+"titleFormatParams:'"+$("#s_titleFormatParams").val()+"',";
			p=""+p+"copyTo:'"+$("#copyTo").combotree("getValues")+"',";
			p=""+p+"linkTo:'"+$("#linkTo").combotree("getValues")+"'}";
			process = $.messager.progress({
                title:'稍后',
                msg:'正在保存基本信息,请稍候...'
            });
            var savePath=path+"/Manage/Content/doAdd.do";
            if(type=="edit"){
            	savePath=path+"/Manage/Content/doEdit.do"
            }
			$.post(
				savePath,
				eval("["+p+"]")[0],
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						process = $.messager.progress({
	                		title:'稍后',
	                		msg:'正在保存正文,请稍候...'
	           			});
						saveContent(res[0].id);
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
		}
	};
	function saveContent(id){
			$.post(
				path+"/Manage/Content/saveText.do",
				{id:id,content:textEditor.getData()},
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						 $.messager.confirm("提示","保存成功,是否继续新建文档?",function(r){if(r){newDoc();}else{close();}});
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
	};
	function newDoc(){ 
		var ss=$("input[mthod]"); 
		for(i=0;i<ss.length;i++){
			if($(ss[i]).attr("name")!="nodeId")
				$(ss[i]).val("");
		}
		textEditor.setData('');
	}; 
	function close(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	function formatTitle(s){
        var formatDialog = $('<div id="formatTitleDlg"/>').appendTo('body');
      
        $(formatDialog).dialog({
        	modal:true,
        	title:'格式化标题',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:500,
        	height:300,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                    	var fval=$("#formatframe")[0].contentWindow.submit();
                        if(fval!=''){
	                        $(formatDialog).dialog("close");
	                        $(formatDialog).remove();
	                        $("#s_titleFormatParams").val("color:"+fval.curColor+";strong:" + fval.strong+";em:"+fval.em+";u:"+fval.u+";size:"+fval.size);
                        }else{
                        	
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){ 
                        $(formatDialog).dialog("close");
	                    $(formatDialog).remove();
                    }
                }], 
        	content:'<iframe id="formatframe" width="480px" height="230px" scrolling="no" frameborder="no" style="overflow:hidden;" src="<%=basePath %>/Manage/Content/formatTitle.do?txt='+$("#s_title").val()+'"></iframe>'
        }); 
        $(formatDialog).dialog("open");
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
     </script>
	</body>
</html>