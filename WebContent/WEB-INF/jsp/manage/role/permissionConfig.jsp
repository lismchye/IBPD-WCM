<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/poshytip-1.2/src/tip-yellow/tip-yellow.css" />
		
			
		<style type="text/css">
		body{
			width:200px;
		}
		table .hlt{
			width:100px;
		}
		table .hl{
			width:200px;
		}
		table .header1 td{
			background-color:#464646;
			border:solid 1px black;
			text-align:center;
			vertical-align:center;
			color:#ffffff;
		}
		table .header2 td{
			background-color:#464646;
			border:solid 1px black;
			text-align:center;
			vertical-align:center;
			color:#ffffff;
			width:100px;
		}
		table .bk{
			border:solid 1px black;
			text-align:center;
			vertical-align:center;
			width:100px;
			background-color:#afafaf;
		}
		table .bk td{
			border:solid 1px black;
			text-align:center;
			vertical-align:center;
			width:100px;
		}
		
		table .body td{
			border:solid 1px black;
			text-align:center;
			vertical-align:center;
			width:100px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.validatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/extValidatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/poshytip-1.2/src/jquery.poshytip.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;"> 
	<div style="position:absolute;left:0;top:0;width:100px;height:30px;z-index:101;">
				<input type="button" onclick="submit()" value="保存配置"/>
				<input type="button" onclick="selectAll()" value="选择所有"/>
		
	</div>
	<div style="position:absolute;left:0;top:0;width:4000px;height:100%;z-index:99;">
		<table cellspacing="0" cellpadding="0">
			<tr class="header1">
				<td rowspan="2" class="hlt">
				</td>
				<c:forEach items="${funMap}" var="fun">
				<td colspan="${fn:length(fun.value)}">
					${fun.key }
				</td>
				</c:forEach>
			</tr>
			<tr class="header2">
				<c:forEach items="${funMap}" var="fun">
				<c:forEach items="${fun.value}" var="f">
				<td>
					${f.funName }
				</td>
				</c:forEach>
				</c:forEach>
			</tr>
			<tr class="body">
				<td class="hl">
					选择
				</td>
				<c:forEach items="${funMap}" var="fun">
				<c:forEach items="${fun.value}" var="f">
				<td>
					<input type="checkbox" funId="${f.id}" modelId="-1"/>
				</td>
				</c:forEach>
				</c:forEach>
			</tr>
				<c:forEach items="${modelList}" var="model">
			<tr class="body">
				<td class="hl">
					${model.modelName}
				</td>
				<c:forEach items="${funMap}" var="fun">
				<c:forEach items="${fun.value}" var="f">
				<td>
					<c:set var="tmp" value="${f.id}-${model.modelId}-${model.modelType}"/>
					
					<input type="checkbox" funId="${f.id}" modelId="${model.modelId}" modelType="${model.modelType}" 
					<c:if test="${confMap[tmp]!=null}"> checked</c:if>
					/>
				</td>
				</c:forEach>
				</c:forEach>
			</tr>
				</c:forEach>
		</table>
	</div>
	<div id="fixHeader" style="position:absolute;left:0;top:0;width:4000px;z-index:100;">
		<table cellspacing="0" cellpadding="0">
			<tr class="header1">
				<td rowspan="2" class="hlt">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<c:forEach items="${funMap}" var="fun">
				<td colspan="${fn:length(fun.value)}">
					${fun.key }
				</td>
				</c:forEach>
			</tr>
			<tr class="header2">
				<c:forEach items="${funMap}" var="fun">
				<c:forEach items="${fun.value}" var="f">
				<td>
					${f.funName }
				</td>
				</c:forEach>
				</c:forEach>
			</tr>
		</table>
	</div>
	</body>
	<script type="text/javascript">
	var path="<%=basePath %>";
	var funIds="";
	var modelIds="";
	var typeIds="";
	<c:forEach items="${confList}" var="conf">
	funIds+="${conf.funId};";
	modelIds+="${conf.modelId};";
	typeIds+="${conf.typeId};";
	</c:forEach>
	
	$(document).ready(function(){
		$("[modelid='-1']").click(function(){
			var chked=$(this).is(":checked");
			var funid=$(this).attr("funid");
			$("input[funid='"+funid+"']").prop("checked",chked);
		});
		//init();
		$(document).scroll(function(){$("#fixHeader").css("top",$(window).scrollTop());});
		//$("table .body").mouseover(function(){$(this).css("background-color","#9f9f9f");});
		//$("table .body").mouseleave(function(){if($(this).css("background-color")=="#eeeeee"){}else{$(this).css("background-color","");}});
		$("table .body").click(function(){if($(this).attr("class")=="bk"){$(this).attr("class","body");}else{$(this).attr("class","bk");}});
	},this);
	function init(){
		var funIdsArray=funIds.split(";");
		var modelIdsArray=modelIds.split(";");
		var typeIdsArray=typeIds.split(";");
		var s=$("input[type='checkbox']");
		s.each(function(index){
			var _s=$(this);
			var t1,t2,t3=false;
			$.each(funIdsArray,function(n,value){
				if(value==_s.attr("funId")){
					t1=true;	
					//break;
				}
			});
			$.each(modelIdsArray,function(n,value){
				if(value==_s.attr("modelId")){
					t2=true;	
					//break;
				}
			});
			$.each(typeIdsArray,function(n,value){
				if(value==_s.attr("modeltype")){
					t3=true;	
					//break;
				}
			});
			if(t1==true && t2==true && t3==true){
				$(_s).prop("checked",true);
			}
		});
	}
	function selectAll(){
		alert("ddd");
		var s=$("input[type='checkbox']");
		s.each(function(index){
			$(this).prop("checked",true);
		});
	}
	function submit(){
		var rtn="";
		var s=$("input:checked");
		s.each(function(index){
			if($(this).attr("modelType")){
				rtn+=$(this).attr("modelId")+":"+$(this).attr("funId")+":"+$(this).attr("modelType")+";";
			}
		});
		if(rtn.length>0){
			rtn=rtn.substring(0,rtn.length-1);
		}
		
			$.ajax({
				 type: "POST",
				 url: path+"/Manage/Role/saveRolePermissionConfig.do?roleId=${roleId}",
				 data: {"confIds":rtn},
				 dataType: "text",
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							alert(t.msg);
							closeCurWindow();
						}else{
							alert(t.msg);
						}
					}
				}
			 });
	};
	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	</script>
	</html>