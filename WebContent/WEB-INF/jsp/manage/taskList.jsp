<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<link href="<%=basePath %>css/default.css" rel="stylesheet" type="text/css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript">
	$(window).resize(function(){ 
		 res();
	}); 
	function res(){
		$('#center').css({ 
			position:'absolute', 
			left: ($(window).width() - $('#center').outerWidth())/2, 
			top: ($(window).height() - $('#center').outerHeight())/2 + $(document).scrollTop() 
		});	
		$('#center').css({top:'50%',left:'50%',margin:'-'+($('#center').height() / 2)+'px 0 0 -'+($('#center').width() / 2)+'px'}); 
		$('#center').css({top:'50%',left:'50%',margin:'-'+($('#center').height() / 2)+'px 0 0 -'+($('#center').width() / 2)+'px'}); 
	};
//初始化函数 
	$(document).ready(function(){
		res();
	});
	</script>
	</head>

	<body style="width:100%;text-align:center;padding:0;margin:0;">
		<!--临时替代方案-->
		<div style="width:100%;height:100%;margin:0 0;position:absolute;left:0;top:0;z-index:100">
			<img src="<%=basePath %>images/tasklist-bg.jpg" style="width:100%;height:100%;margin:0 0;"/>
		</div>
		<div id="center" style="width:450px;height:150px;z-index:200">
			<img src="<%=basePath %>images/tasklist-center.png" style="width:100%;height:100%;margin:0 0;"/>
		</div>
	</body>
</html>
