package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.assist.PermissionModelEntity;
import com.ibpd.henuocms.common.DateFormatUtil;
import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.Pinyin4jUtil;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.common.calender.YangLiToNongLiUtil;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.nodeForm.INodeFormService;
import com.ibpd.henuocms.service.nodeForm.NodeFormServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;

@Controller
public class Index extends BaseController {
	@RequestMapping("Manage/index.do")
	public String index(org.springframework.ui.Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("header_text",IbpdCommon.getInterface().getPropertiesValue("header_text"));
		model.addAttribute("footer_text",IbpdCommon.getInterface().getPropertiesValue("footer_text"));
		model.addAttribute("system_fullname",IbpdCommon.getInterface().getPropertiesValue("system_fullname"));
		model.addAttribute("system_smallname",IbpdCommon.getInterface().getPropertiesValue("system_smallname"));
		model.addAttribute("loginedUserName",SysUtil.getCurrentLoginedUserName(req.getSession()));
		model.addAttribute("loginedUserType",SysUtil.getCurrentLoginedUserType(req.getSession()));
		model.addAttribute("permissionEnable_userDisp",SysUtil.checkPermissionIsEnable(req.getSession(), "user_disp", -1L,PermissionModelEntity.MODEL_TYPE_FUNMODEL));
		model.addAttribute("permissionEnable_roleDisp",SysUtil.checkPermissionIsEnable(req.getSession(), "role_disp", -1L,PermissionModelEntity.MODEL_TYPE_FUNMODEL));
		model.addAttribute("permissionEnable_paramDisp",SysUtil.checkPermissionIsEnable(req.getSession(), "param_disp", -1L,PermissionModelEntity.MODEL_TYPE_FUNMODEL));
		YangLiToNongLiUtil s=new YangLiToNongLiUtil();
		model.addAttribute("current_calender","今天是"+DateFormatUtil.formatToYYYYMMDD(new Date())+","+s.cyclical()+s.animalsYear()+"年"+s.toString());
		return "manage/index";
	}
	@RequestMapping("Manage/taskList.do")
	public String taskList(org.springframework.ui.Model model) throws IOException{
		return "manage/taskList";
	}
	@RequestMapping("Manage/init.do")
	public String init(org.springframework.ui.Model model,HttpServletRequest req) throws IOException{
		INodeFormService formServ=(INodeFormService) getService(NodeFormServiceImpl.class);
		formServ.initDefaultNodeForm();
		formServ.initDefaultContentForm();
		IPageTemplateService tempServ=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		tempServ.init();
		ISubSiteService siteServ=(ISubSiteService) getService(SubSiteServiceImpl.class);
		List<SubSiteEntity> siteList=siteServ.getList();
		for(SubSiteEntity s:siteList){
			siteServ.deleteByPK(s.getId());
		}
		SubSiteEntity se=new SubSiteEntity();
		se.setFullName("食尚功夫网上商城");
		se.setIsLocked(false);
		se.setSiteName("食尚功夫网上商城");
		
		se.setDirectory(Pinyin4jUtil.getPinYinHeadChar(se.getSiteName()));
		siteServ.saveEntity(se);
		return index(model,req);
	}
}
