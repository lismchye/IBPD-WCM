package com.ibpd.henuocms.common;

import java.util.HashMap;
import java.util.Map;
/**
 * 生成静态页的实体类，用来对要生成url静态页面的参数进行说明
 * @author mg
 *
 */
public class StaticPageEntity {

	private String url="";
	private String fileName="";
	private Map<String,String> urlParams=new HashMap<String,String>();
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFileName() {
		return fileName; 
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Map<String, String> getUrlParams() {
		return urlParams;
	}
	public void setUrlParams(Map<String, String> urlParams) {
		this.urlParams = urlParams;
	}
	public void addParams(String pName,String pValue){
		this.urlParams.put(pName, pValue);
	}
}
